package GUI;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.Dimension;

public class mainFrame extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2418226089178878967L;

	public mainFrame() {
		
		super("Gamification");
		setMinimumSize(new Dimension(250, 290));
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		getContentPane().setLayout(null);
		
		JButton btnCriarTask = new JButton("New Task");
		btnCriarTask.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				setVisible(false);
				newTask task = new newTask();
				task.setVisible(true);
			}
		});
		btnCriarTask.setBounds(66, 50, 130, 23);
		getContentPane().add(btnCriarTask);
		
		JButton btnVerDashboard = new JButton("Open Dashboard");
		btnVerDashboard.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				setVisible(false);
				new InterfaceTrainer();
			}
		});
		btnVerDashboard.setBounds(66, 85, 130, 23);
		getContentPane().add(btnVerDashboard);
		
		JLabel lblTrainer = new JLabel("Trainer");
		lblTrainer.setBounds(30, 30, 46, 14);
		getContentPane().add(lblTrainer);
		
		JLabel lblLearner = new JLabel("Learner");
		lblLearner.setBounds(30, 119, 46, 14);
		getContentPane().add(lblLearner);
		
		JButton btnVerDashboard_1 = new JButton("Open Dashboard");
		btnVerDashboard_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		btnVerDashboard_1.setBounds(66, 144, 130, 23);
		getContentPane().add(btnVerDashboard_1);
		
		JButton btnExit = new JButton("Exit");
		btnExit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				dispose();
				System.exit(0);
			}
		});
		btnExit.setBounds(85, 221, 89, 23);
		getContentPane().add(btnExit);
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		mainFrame gui = new mainFrame();
		
		gui.setVisible(true);
		
	}
}
