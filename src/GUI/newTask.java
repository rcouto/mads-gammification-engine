package GUI;

import javax.swing.JFrame;
import javax.swing.JButton;
import javax.swing.JLabel;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import javax.swing.JTextField;
import java.awt.Dimension;

public class newTask extends JFrame {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3085553118899769292L;
	private JTextField textField;
	
	public newTask() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setMinimumSize(new Dimension(300, 125));
		getContentPane().setLayout(null);
		
		JLabel lblTask = new JLabel("Task: ");
		lblTask.setBounds(10, 24, 46, 14);
		getContentPane().add(lblTask);
		
		textField = new JTextField();
		textField.setBounds(60, 21, 220, 20);
		getContentPane().add(textField);
		textField.setColumns(10);
		
		JButton btnSubmit = new JButton("Submit");
		btnSubmit.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO
			}
		});
		btnSubmit.setBounds(60, 52, 101, 23);
		getContentPane().add(btnSubmit);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//TODO 
				System.exit(0);
			}
		});
		btnCancel.setBounds(171, 52, 109, 23);
		getContentPane().add(btnCancel);
	}
}
