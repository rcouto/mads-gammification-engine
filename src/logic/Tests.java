package logic;
import static org.junit.Assert.*;
import junit.framework.TestCase;

import java.util.Iterator;
import java.util.List;
import java.util.Map;


public class Tests extends TestCase {

	public void testTrainer() {
		Trainer t1 = new Trainer("Lu�s Guilherme");
		assertEquals(t1.getName(), "Lu�s Guilherme");
		assertNotEquals(t1.getName(), "Luis Guilherme");
	}
	
	public void testAchievement() {
		Achievement a1 = new Achievement("description");
		assertEquals(a1.getDescription(), "description");
		assertNotEquals(a1.getDescription(), "desc ription");
		assertEquals(a1.isDone(), false);
		a1.setDone(true);
		assertEquals(a1.isDone(), true);
		assertEquals(a1.getPoints(), 0);
		a1.setPoints(5);
		assertEquals(a1.getPoints(), 5);
	}
	
	public void testLearner() {
		Learner l1 = new Learner("Rui Couto");
		assertEquals(l1.getName(), "Rui Couto");
		assertNotEquals(l1.getName(), "RuiCouto");
		assertEquals(l1.getPoints(), 0);
		
	}
		
	public void testLearners() {
		assertEquals(Learners.getInstance().getLearnersList().isEmpty(), true);
		Learners.getInstance().addLearner(new Learner("Rui Couto"));
		assertEquals(Learners.getInstance().getLearnersList().size(), 1);
		assertEquals(Learners.getInstance().getAchievements(new Learner("Rui Couto")).isEmpty(), true);
		Learners.getInstance().addAchievement(new Learner("Rui Couto"), new Achievement("test4"));
		assertEquals(Learners.getInstance().getAchievements(new Learner("Rui Couto")).size(), 1);
	}

	public void testAchievementCreation() {
		Trainer t = new Trainer("trainer");
		t.createAchievement("Ach1");
		
		Map<Learner, List<Achievement>> map = Learners.getInstance().getLearnersList();
		Iterator<Map.Entry<Learner, List<Achievement>>> entries = map.entrySet().iterator();
		
		while (entries.hasNext()) {
			Map.Entry<Learner, List<Achievement>> entry = entries.next();
			List<Achievement> learnerAchievements = entry.getValue();
			assertTrue(learnerAchievements.contains(new Achievement("Ach1")));
		}
	}
}

