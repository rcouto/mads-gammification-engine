package logic;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

public class Trainer {
	String name;

	public Trainer(String name) {
		super();
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void createAchievement(String achievementDescription) {
		Map<Learner, List<Achievement>> map = Learners.getInstance().getLearnersList();
		Iterator<Map.Entry<Learner, List<Achievement>>> entries = map.entrySet().iterator();
		
		while (entries.hasNext()) {
			Map.Entry<Learner, List<Achievement>> entry = entries.next();
			Learner learner = entry.getKey();
			Learners.getInstance().addAchievement(learner, new Achievement(achievementDescription));
		}
	}
}
