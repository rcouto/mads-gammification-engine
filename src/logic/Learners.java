package logic;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


public class Learners {
	private static Learners instance = null;
	private Map<Learner, List<Achievement>> learnersList;
	   protected Learners() {
		   learnersList = new HashMap<Learner, List<Achievement>>();
	   }
	   public static Learners getInstance() {
	      if(instance == null) {
	         instance = new Learners();
	      }
	      return instance;
	   }
	   public void addLearner (Learner l1) {
		   learnersList.put(l1, new ArrayList<Achievement>());
	   }
	   
	   public List<Achievement> getAchievements(Learner l1) {
		   return learnersList.get(l1);
	   }
	   
		public Map<Learner, List<Achievement>> getLearnersList() {
			return learnersList;
		}
		
		public void addAchievement(Learner l1, Achievement a1) {
			getAchievements(l1).add(a1);
		}

}
